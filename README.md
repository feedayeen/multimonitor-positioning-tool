The built in windows display settings tool allows users to customize the monitor's virtual configuration and various display settings visually. This configuration however is not persistent. A full screen video game or rebooting the computer can cause the virtual configuration to change forcing the user to manually rearrange the displays again.

The first time that this tool is run it will create a configuration file in the same directory which copies the current monitor settings to a CSV file. If the displays change, the user should launch the tool again and it will load the configuration from this file reverting the changes.

The user can also adjust the configuration file manually after it has been created to force changes.