#include <windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <cstdio>
#include <sstream>

using namespace std;

/*
 * This stores the complete state for a given monitor
 */
struct Monitor
{
	DISPLAY_DEVICE dd;
	DEVMODE dm;
	Monitor(DISPLAY_DEVICE display, DEVMODE mode)
	{
		dd = display;
		dm = mode;
	}

	void print()
	{
		cout << dd.DeviceName;
		cout << " (" << dm.dmPelsWidth << "," << dm.dmPelsHeight << ") (" << dm.dmPosition.x << "," << dm.dmPosition.y << ")" << endl;
		cout << "\t" << endl;
	}
};

/*
 * Applies the to a given monitor
 */
void changeSettings(Monitor monitor)
{
	long result;
	if (monitor.dd.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE)//Primary Monitor
	{
		result = ChangeDisplaySettingsEx(monitor.dd.DeviceName, &monitor.dm, NULL, CDS_SET_PRIMARY, NULL);
	}
	else
	{
		result = ChangeDisplaySettingsEx(monitor.dd.DeviceName, &monitor.dm, NULL, 0, NULL);
	}
	if (result != DISP_CHANGE_SUCCESSFUL)
	{
		cout << endl << "CHANGE FAILED FOR " << monitor.dd.DeviceName << endl;
	}
	else
	{
		cout << "Change to " << monitor.dd.DeviceName << " Successful" << endl;
	}
}

/*
 * Creates a new CSV file if none exist.
 * The CSV file represents the current monitor configuration.
 * The user can later adjust the configuration in the CSV file manually and changes will be saved
 */
void writeCSV(vector<Monitor> monitors)
{
	ifstream file("settings.csv");
	if(file)
	{
		cout << "File found, not writing current configuration" << endl;
		return;
	}
	cout << "Writing settings to file" << endl;
	ofstream out;
	out.open("settings.csv");
	out << "name,width,height,orientation,x position,y position,primary" << endl;
	for (vector<Monitor>::iterator it = monitors.begin(); it != monitors.end(); ++it)
	{
		Monitor mon = *it;
		stringstream text;
		text << mon.dd.DeviceName << "," << mon.dm.dmPelsWidth << "," << mon.dm.dmPelsHeight << ","
				<< mon.dm.dmDisplayOrientation << "," << mon.dm.dmPosition.x << "," << mon.dm.dmPosition.y << ","
				<< ((mon.dd.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE)!=0);
		string output = text.str();
		out << output << endl;
		cout << output << endl;
	}
	out.close();
	cout << "settings file closed" << endl;
}

/*
 * Parses the first entry in a comma deliminated string as an integer.
 * Removes the first entry from the string including the comma
 * Returns the integer value
 */
int parseInt(string &line)
{
	string value = line.substr(0,line.find(","));
	line = line.substr(line.find(",")+1,line.size());
	return atoi(value.c_str());
}

/*
 * Updates the array of monitors to include the changes found in the CSV file
 * If an entry in the CSV doesn't exist in the display list, it is ignored
 */
vector<Monitor> readCSV(vector<Monitor> monitors)
{
	cout << "Reading settings from file" << endl;
	ifstream in;
	in.open("settings.csv");
	string line;
	getline(in, line, '\n');
	while(getline(in, line, '\n'))
	{
		stringstream csv(line);
		string name = line.substr(0,line.find(","));
		line = line.substr(line.find(",")+1,line.size());
		for (vector<Monitor>::iterator it = monitors.begin(); it != monitors.end(); ++it)
		{
			if(it->dd.DeviceName == name)
			{
				cout << "Updating " << name << endl;
				it->dm.dmPelsWidth = parseInt(line);
				it->dm.dmPelsHeight = parseInt(line);
				it->dm.dmDisplayOrientation = parseInt(line);
				it->dm.dmPosition.x = parseInt(line);
				it->dm.dmPosition.y = parseInt(line);
				if(parseInt(line))//Set primary display bit high
				{
					it->dd.StateFlags |= DISPLAY_DEVICE_PRIMARY_DEVICE;
				}
				else//Primary display bit is low
				{
					it->dd.StateFlags &=~ DISPLAY_DEVICE_PRIMARY_DEVICE;
				}
				break;
			}
		}
	}
	in.close();
	return monitors;
}

int main()
{
	cout << "Started Monitor Repair Tool" << endl;
	DISPLAY_DEVICE dd;
	memset(&dd, 0, sizeof(DISPLAY_DEVICE));
	dd.cb = sizeof(DISPLAY_DEVICE);
	DEVMODE dm;
	vector<Monitor> monitors;
	for (int index = 0; EnumDisplayDevices(NULL, index, &dd, 0); index++)
	{
		if (dd.StateFlags & DISPLAY_DEVICE_ACTIVE)
		{
			memset(&dm, 0, sizeof(dm));
			dm.dmSize = sizeof(dm);
			EnumDisplaySettings(dd.DeviceName, ENUM_CURRENT_SETTINGS, &dm);
			monitors.push_back(Monitor(dd, dm));
		}
	}
	writeCSV(monitors);
	monitors = readCSV(monitors);
	for (vector<Monitor>::iterator it = monitors.begin(); it != monitors.end(); ++it)
	{
		changeSettings(*it);
	}
}
